from bokeh.plotting import figure, output_file, show 
output_file("index_culito.html")  
           
graph = figure(title = "Bokeh Pie Chart")  
x = 0
y = 0
radius = 1
start_angle = [0, 1.8, 2.5, 
               3.7, 5.6] 
end_angle = [1.8, 2.5, 3.7, 
             5.6, 0] 
color = ["violet", "blue", "green", 
          "yellow", "red"] 
graph.wedge(x, y, radius, 
            start_angle, 
            end_angle, 
            color = color) 
show(graph)